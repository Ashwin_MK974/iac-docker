#!/bin/bash
source .env
function docker__settings()
{
    #Inclusion de require.sh pour mettre à jour la version docker-compose afin d'apcepter l'argument "--profile"
    chmod +x $DATA_PATH_STATE_REPO/require.sh
    bash $DATA_PATH_STATE_REPO/require.sh


    PID=$(pgrep dockerd)
    if [[ -z "$PID" ]]
    then
            echo "Démarrage de docker"
            dockerd --data-root $DATA_PATH_STATE_REPO/images > /dev/null 2>&1 &
            echo " Répertoire de base Docker "
            docker info | grep "Docker Root Dir"
    else
    echo -e "Docker est déjà en cours d'exécution\n Redemarrage"
    kill $PID
    sleep 10
    



    export DATA_PATH_STATE_REPO=$PWD #Mise à jour de la variable $DATA_PATH_STATE_REPO en fonction de la où on se situe via PWD
    if [[ "$1" == "" || "$1" == "--default" ]]
    then
        dockerd  > /dev/null 2>&1 &
    elif [[ "$1" == "--personal" ]]
    then
        dockerd --data-root $DATA_PATH_STATE_REPO/images > /dev/null 2>&1 & #Si on lance avec l'option personal, les images seront persisté et stocké dans /home mais limité en terme d'image
    fi
    


    echo " Répertoire de base Docker : "
    sleep 5
    docker info | grep "Docker Root Dir"
    fi
}
function init()
{
    chmod -R 777 $PWD
    echo "" > /root/.bashrc
    export PATH_INSTALL=$PWD
    export DATA_PATH_STATE_REPO=$PWD
    wget -q https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/stackDeployment.sh
    wget -q --timestamping -P /usr/bin https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/dockstack
    chmod +x  "$PWD/dockstack/stackDeployment.sh"
    bash "$PWD/dockstack/stackDeployment.sh"
    source /root/.bashrc
    chmod +x /usr/bin/dockstack

}
export PATH_INSTALL=$PWD
export DATA_PATH_STATE_REPO=$PWD
if [[ "$1" == "" || "$1" == "--normal-launch" ]]
then 
    init
elif [[ "$1" == "--docker-setting" ]]
then 
    echo "Réglage des paramètres docker : Définitions du répertoire par défauts"
    docker__settings
elif [[ "$1" == "--docker-setting-personal" ]]
then 
     echo "Réglage des paramètres docker : Définitions du répertoire par défauts dans $PWD/images"
    docker__settings --personal
elif [[ "$1" == "--ngrok-token" ]]
then 
    echo "Configuration de Ngrok"
    #Pour l'exposition des services sinon à commenter
    ngrok config add-authtoken $NGROK_TOKEN
elif [[ "$1" == "--direct-launch" || "$1" == "--direct-launch-personal" ]]
then 
    
    echo "root:$STACK_PASSWORD" | sudo chpasswd
    echo "$STACK_PASSWORD" | sudo -S bash -c "
        export PATH_INSTALL=$PWD
        export DATA_PATH_STATE_REPO=$PWD
        chmod -R 777 $PWD
        wget -q https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/stackDeployment.sh
        wget -q --timestamping -P /usr/bin https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/dockstack
        chmod +x  "$PWD/dockstack/stackDeployment.sh"
        bash "$PWD/dockstack/stackDeployment.sh"
        chmod +x /usr/bin/dockstack
        bash "$PWD/dockstack/stackDeployment.sh"
        source /root/.bashrc
        if [[ $1 == "--direct-launch" ]]
        then
            bash commands.sh --docker-setting
        elif [[ $1 == "--direct-launch-personal" ]]
        then
            bash commands.sh --docker-setting-personal
        fi
        bash $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh dockstack --v
        bash $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh dockstack mount services
        bash commands.sh --ngrok-token
        bash $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh dockstack expose-services start
        
    "
fi
