#!/bin/bash

# Source du script externe
source dockstack/stackDeployment.sh

HASHES_FILE="$1/processed_hashes.txt"

if [[ -e "$HASHES_FILE" ]]
then
        echo "Le fichier processed_hashes.txt existe déjà." 
else
    echo "Initialisation"
    touch "$HASHES_FILE"
fi

hash_exists() {
    local file_hash=$1
    grep -q "$file_hash" "$HASHES_FILE"
}

# Fonction pour ajouter le hachage du fichier à processed_hashes.txt
add_hash() {
    local file_hash=$1
    echo "$file_hash" >> "$HASHES_FILE"
}

# Fonction pour insérer des fichiers depuis un répertoire donné
insert_files_from_directory() {
    TARGET_DIRECTORY=$1
    ELASTIC_INDEX=$2

    shopt -s nullglob 
    for file in $TARGET_DIRECTORY/*.csv; do
        file_hash=$(sha256sum "$file" | awk '{print $1}')
        
        if hash_exists "$file_hash"; then
            echo "Skipping already processed file: $file"
            continue
        fi
        
        json_filename="$TARGET_DIRECTORY/$(basename "$file" .csv).json"
        python3 transform.py "$file" "$json_filename"
        insert_data "$json_filename" "$ELASTIC_INDEX"
        add_hash "$file_hash"
    done
    shopt -u nullglob
}

MONITOR_DIR="$1"
ELASTIC_INDEX="$2"

if [[ -z "$ELASTIC_INDEX" ]]; then
    echo "Veuillez fournir un index Elasticsearch comme deuxième argument."
    exit 1
fi

if [[ ! -d "$MONITOR_DIR" ]]; then
    echo "Le répertoire $MONITOR_DIR n'existe pas!"
    exit 1
fi

echo "Surveillance du répertoire: $MONITOR_DIR"

inotifywait -m -e create --format '%w%f' "${MONITOR_DIR}" | while read NEW_FILE; do
    if [[ $NEW_FILE == *.csv ]]; then
        echo "Un nouveau fichier CSV a été détecté : ${NEW_FILE}"
        insert_files_from_directory "$MONITOR_DIR" "$ELASTIC_INDEX"
    fi
done
