services:
  elasticsearch:
    container_name: elasticsearch
    image: docker.elastic.co/elasticsearch/elasticsearch:8.9.0
    environment:
      - discovery.type=single-node
    volumes:
      - ${PATH_INSTALL}/esdata:/usr/share/elasticsearch/data
      - ./elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    ports:
      - 9200:9200
    profiles:
      - required

  kibana:
    container_name: kibana
    image: docker.elastic.co/kibana/kibana:8.9.0
    depends_on:
      - elasticsearch
    volumes:
      - ./kibana.yml:/usr/share/kibana/config/kibana.yml
      - ${PATH_INSTALL}/kibanadata:/usr/share/kibana/data
    ports:
      - 5601:5601
    profiles:
      - required

  logstash:
    container_name: logstash
    image: docker.elastic.co/logstash/logstash:8.9.0
    depends_on:
     - elasticsearch
    volumes:
      - ${PATH_INSTALL}/logstash/config/logstash.yml:/usr/share/logstash/config/logstash.yml
      - ${PATH_INSTALL}/logstash/pipeline:/usr/share/logstash/pipeline  
      - ${PATH_INSTALL}/logstash/config/Example.csv:/usr/share/logstash/Example.csv  
    ports:
      - 9600:9600 
    profiles:
      - configured


  grafana:
    container_name: grafana 
    image: grafana/grafana-enterprise
    volumes:
      - ${PATH_INSTALL}/grafanadata:/var/lib/grafana
      - ./datasources.yaml:/etc/grafana/provisioning/datasources/datasources.yaml
      - ./grafana.ini:/etc/grafana/grafana.ini
    ports:
      - 3005:3000
    profiles:
      - required

  prometheus:
    container_name: prometheus
    image: prom/prometheus
    volumes:
      - ${PATH_INSTALL}/prometheusdata:/prometheus
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - 9090:9090
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.enable-lifecycle'
    networks:
    - prometheus-net
    profiles:
      - required


  mongodb:
    container_name: mongodb
    image: mongo:latest
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${MONGO_INITDB_ROOT_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_INITDB_ROOT_PASSWORD}
    ports:
      - "21021:27017"
    volumes:
      - ${PATH_INSTALL}/mongodb-data:/data/db
    profiles:
      - required
      

  nginx:
    container_name: nginx
    image: nginx:latest
    restart: always
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports:
      - 8080:80
      - 27020:27020
    networks:
      - default
      - gitlab-network
      - kong-net
      - prometheus-net
    profiles:
      - required

  

  nginx-exporter:
    container_name: nginx-exporter
    image: nginx/nginx-prometheus-exporter:latest
    ports:
      - "9113:9113"
    command:
      - "-nginx.scrape-uri=http://nginx:8080/nginx_status"
    depends_on:
      - nginx
    profiles:
      - required

  kong-database:
    image: postgres:13
    environment:
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_DB: ${POSTGRES_DATABASE}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
    volumes:
      - ${PATH_INSTALL}/kong-database:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U ${POSTGRES_USER} -d ${POSTGRES_DATABASE}"]
      interval: 10s
      timeout: 5s
      retries: 5
    networks:
      - kong-net

  kong-migrations:
    container_name: kong_migration
    image: kong:latest
    command: sh -c "kong migrations bootstrap && kong migrations up && kong migrations finish"
    environment:
      KONG_DATABASE: postgres
      KONG_PG_HOST: kong-database
      KONG_PG_USER: ${POSTGRES_USER}
      KONG_PG_PASSWORD: ${POSTGRES_PASSWORD}
      KONG_PG_DATABASE: ${POSTGRES_DATABASE}
    depends_on:
      - kong-database
    networks:
      - kong-net

  kong:
    container_name: kong
    image: kong:latest
    environment:
      KONG_DATABASE: postgres
      KONG_PG_HOST: kong-database
      KONG_PG_USER: ${POSTGRES_USER}
      KONG_PG_PASSWORD: ${POSTGRES_PASSWORD}
      KONG_PG_DATABASE: ${POSTGRES_DATABASE}
      KONG_PROXY_ACCESS_LOG: /dev/stdout
      KONG_ADMIN_ACCESS_LOG: /dev/stdout
      KONG_PROXY_ERROR_LOG: /dev/stderr
      KONG_ADMIN_ERROR_LOG: /dev/stderr
      KONG_ADMIN_LISTEN: 0.0.0.0:8001, 0.0.0.0:8444 ssl
    ports:
      - "8000:8000"
      - "8001:8001"
      - "8443:8443"
      - "8444:8444"
    depends_on:
      - kong-migrations
    networks:
      - kong-net

  vector:
    container_name: vector
    build: ./vector
    volumes:
      - ./vector.toml:/etc/vector/vector.toml 
      - ${PATH_INSTALL}:/vector_data 
      - ./transform_vector.py:/scripts/transform_vector.py
      - ${PATH_INSTALL}/smsc_data:/data/smsc_data
      - ${PATH_INSTALL}/train.csv:/data/train_data/train.csv
    profiles:
      - configured

  gitlab:
    container_name: gitlab
    image: gitlab/gitlab-ce:latest
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab/gitlab'
    volumes:
      - ${PATH_INSTALL}/gitlab-config:/etc/gitlab
      - ${PATH_INSTALL}/gitlab-logs:/var/log/gitlab
      - ${PATH_INSTALL}/gitlab-data:/var/opt/gitlab
    ports:
      - "8085:80"
    networks:
      - gitlab-network
    profiles:
      - required

  gitlab-runner:
    container_name: gitlab-runner
    image: gitlab/gitlab-runner:latest
    depends_on:
      - gitlab
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${PATH_INSTALL}/gitlab-runner-config:/etc/gitlab-runner
      - /usr/bin/docker:/usr/bin/docker
    networks:
      - gitlab-network
    profiles:
      - required


networks:
  gitlab-network:
    driver: bridge
  kong-net:
    driver: bridge
  prometheus-net:
    driver: bridge


