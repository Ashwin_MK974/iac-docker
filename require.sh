#!/bin/bash
#Author AshwinMK
#Script d'installation de Docker & Docker-Compose
if ! command -v docker &> /dev/null; then
    echo "Docker n'est pas installé. L'installation va commencer."
    
    sudo apt-get update -y

    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    sudo apt-key fingerprint 0EBFCD88

    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

    sudo apt-get update

    sudo apt-get install -y docker-ce

    sudo systemctl enable docker

    sudo systemctl start docker

    sudo usermod -aG docker $USER

    sudo systemctl restart docker
    
    docker version

    echo "Docker a été installé avec succès !"

else
    echo "Docker est déjà installé sur cette machine."
fi

#!/bin/bash

# Vérifie si jq est installé
if ! command -v jq &> /dev/null; then
    echo "jq n'est pas installé. L'installation va commencer."
    
    # Installe jq
    sudo apt-get install -y jq

    # Vérifie l'installation de jq
    if command -v jq &> /dev/null; then
        echo "jq a été installé avec succès !"
    else
        echo "L'installation de jq a échoué."
    fi

else
    echo "jq est déjà installé sur cette machine"
fi


if ! command -v docker-compose &> /dev/null; then
    echo "Docker Compose n'est pas installé. L'installation va commencer."
   
    sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

    sudo chmod +x /usr/local/bin/docker-compose
    echo "Docker Compose a été installé avec succès."
else
    echo "Docker Compose est déjà installé."
    echo "Vérification de la version"
    CURRENT_VERSION=$(docker-compose version --short 2>/dev/null | sed 's/\.//g' )
    TARGET_VERSION=$(echo "2.27.0" | sed 's/\.//g')
    echo "Version Actuelle de Docker Compose" :  $(docker-compose version --short)
    if [[ "$CURRENT_VERSION" -lt "$TARGET_VERSION" ]]
    then
        echo "Mise à jour nécessaire de Docker Compose vers la version" $TARGET_VERSION
        curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &&
        chmod +x /usr/local/bin/docker-compose &&
        docker-compose -v
    else
        echo "Aucune mise à jour nécessaire."
    fi
fi