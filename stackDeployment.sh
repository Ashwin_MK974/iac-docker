#!/bin/bash
#Author AMK
DATA_PATH_STATE_REPO=$PWD
VERSION=$'Version STABLE 1.0.9\n
Fix Dos2Unix output\n
Fix Ngrok Verif\n
Auto Install Docker & Compose\n
Last Docker-Compose Version \n
Add network to Nginx for operate Kong Communication \n
Add permission  \n 
Include require.sh in update \n
1.0.9 : Add feature to restart "required" container \n
1.0.9 : Fix Manage
1.0.10: Fix Docker Settings
'
verify_and_create() {
    local directory_path="$1"
    if [ -d "$directory_path" ]
	 then
        echo "Le dossier existe : $directory_path"
    else
        echo "Création du dossier : $directory_path"
        mkdir -p "$directory_path"
    fi
}
function verify_existant_element__v3(){
    if [ -$1 $2 ]
    then
    if [ "$5" == "--v" ]
    then
        success_write "L'élement $2 existe"
        if [ "$6" != "" ]
        then
          echo $6
        fi
    fi
        return $3
    else
    if [ "$5" == "--v" ]
    then
        fail_write "L'élement $2 n'existe pas"
        if [ "$6" != "" ]
        then
          echo $6
        fi
    fi

        return $4
    fi
}
function success_write(){
    echo ""
    echo -e "\e[1m$1 ✔️\e[0m"
    echo -e "\e[97m"
}
function fail_write(){
   echo ""
   echo -e "\e[1m$1 ❌\e[0m"
   echo -e "\e[97m"
}
function download()
{
	wget -q -O $1 $2
}
function getFile()
{
            echo "Téléchargement des fichiers requis"
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/.env
            chmod 777 $DATA_PATH_STATE_REPO/.env
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/docker-compose.yml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/datasources.yaml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/kibana.yml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/elasticsearch.yml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/prometheus.yml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/grafana.ini
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/ngrok.yml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/nginx.conf
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/grafana.ini
            wget -q --timestamping -P $DATA_PATH_STATE_REPO/vector https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/Dockerfile
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/vector.toml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/train.csv
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/transform.py
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/transform_vector.py
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/commands.sh
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/backup.sh
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/watch.sh
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/kubernetes_cluster.yml
            wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/require.sh
            chmod +x  $DATA_PATH_STATE_REPO/transform.py  $DATA_PATH_STATE_REPO/transform_vector.py  $DATA_PATH_STATE_REPO/commands.sh  $DATA_PATH_STATE_REPO/backup.sh $DATA_PATH_STATE_REPO/watch.sh
}
function downloadFile()
{
        if [[ "$1" == "" ]]
        then 
            getFile
        elif [[ "$1" == "--all" ]]
        then
            getFile
        else
            echo "download Commands Error"
        fi           
}


function loop_deploy()
{
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/esdata" 0 1 && rm -r $DATA_PATH_STATE_REPO/esdata
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/esconfig" 0 1 && rm -r $DATA_PATH_STATE_REPO/esconfig
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/kibanadata" 0 1 && rm -r $DATA_PATH_STATE_REPO/kibanadata
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/kibanaconfig" 0 1 && rm -r $DATA_PATH_STATE_REPO/kibanaconfig
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/grafanadata" 0 1 && rm -r $DATA_PATH_STATE_REPO/grafanadata
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/certs" 0 1 && rm -r $DATA_PATH_STATE_REPO/certs
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/mongodb-data" 0 1 && rm -r $DATA_PATH_STATE_REPO/mongodb-data
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/mongo-express-config" 0 1 && rm -r $DATA_PATH_STATE_REPO/mongo-express-config
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/mongo-express-config" 0 1 && rm -r $DATA_PATH_STATE_REPO/mongo-express-config
                verify_existant_element__v3 d "$DATA_PATH_STATE_REPO/vector" 0 1 && rm -r $DATA_PATH_STATE_REPO/vector

            
                verify_and_create "$DATA_PATH_STATE_REPO/esdata"
                verify_and_create "$DATA_PATH_STATE_REPO/esconfig"
                verify_and_create "$DATA_PATH_STATE_REPO/kibanadata"
                verify_and_create "$DATA_PATH_STATE_REPO/kibanaconfig"
                verify_and_create "$DATA_PATH_STATE_REPO/grafanadata"
                verify_and_create "$DATA_PATH_STATE_REPO/certs"
                verify_and_create "$DATA_PATH_STATE_REPO/mongodb-data"
                verify_and_create "$DATA_PATH_STATE_REPO/mongo-express-config"
                verify_and_create "$DATA_PATH_STATE_REPO/vector"
                downloadFile
                chmod -R 777 $DATA_PATH_STATE_REPO
}
function deploy_stack()
{
    chmod -R 777 $DATA_PATH_STATE_REPO/* #Ajout suite à l'erreur de démarrage du container de Prometheus
    if [ "$1" == "" ]
    then
            echo "Déploiement des instance par défauts"
            verify_and_create $DATA_PATH_STATE_REPO
            if [ -d $DATA_PATH_STATE_REPO ]
            then
                loop_deploy
                chmod -R 777 $DATA_PATH_STATE_REPO
                docker-compose --profile required up -d
                sleep 60 &&
                echo "Test de la connexion ElasticSearch ..."
                curl http://localhost:9200
            else
                echo "Le dossier n'existe pas"
            fi
    elif [ "$1" == "reset" ]
    then
       echo "Arrêt des containers de la stack"
         docker-compose --profile required down
       echo "Redeploiement des containers ..."
         docker-compose --profile required up
    elif [ "$1" == "from-cloud" ]
    then
        loop_deploy
        bash $DATA_PATH_STATE_REPO/backup.sh get backup &&
        chmod -R 777 $DATA_PATH_STATE_REPO
        docker-compose --profile required up -d &&
        sleep 60 &&
        echo "Test de la connexion ElasticSearch ..."
        curl http://localhost:9200
    elif [ "$1" == "--secure-stack" ]
    then
            verify_and_create $DATA_PATH_STATE_REPO
            if [ -d $DATA_PATH_STATE_REPO ]
            then
                downloadFile
                verify_existant_element__v3 d /root/esdata 0 1 && rm -r $DATA_PATH_STATE_REPO/esdata
                verify_existant_element__v3 d /root/esdata 0 1 && rm -r $DATA_PATH_STATE_REPO/esconfig
                verify_existant_element__v3 d /root/esdata 0 1 && rm -r $DATA_PATH_STATE_REPO/kibanadata
                verify_existant_element__v3 d /root/esdata 0 1 && rm -r $DATA_PATH_STATE_REPO/kibanaconfig
                verify_existant_element__v3 d /root/esdata 0 1 && rm -r $DATA_PATH_STATE_REPO/grafanadata
                verify_existant_element__v3 d /root/esdata 0 1 && rm -r $DATA_PATH_STATE_REPO/certs
                verify_and_create "$DATA_PATH_STATE_REPO/esdata"
                verify_and_create "$DATA_PATH_STATE_REPO/esconfig"
                verify_and_create "$DATA_PATH_STATE_REPO/kibanadata"
                verify_and_create "$DATA_PATH_STATE_REPO/kibanaconfig"
                verify_and_create "$DATA_PATH_STATE_REPO/grafanadata"
                verify_and_create "$DATA_PATH_STATE_REPO/certs"
                chmod 777 $DATA_PATH_STATE_REPO
                docker-compose --profile required up -d
                sleep 60 && echo 'y' | docker exec -i elasticsearch bin/elasticsearch-reset-password -u elastic > $DATA_PATH_STATE_REPO/elastic &&

                CERTIFICATE_PATH="/usr/share/elasticsearch/certificates"
                docker exec -i elasticsearch bin/elasticsearch-certutil ca --pem -s --out $CERTIFICATE_PATH/my_ca.zip && 
                docker exec -i elasticsearch unzip -j $CERTIFICATE_PATH/my_ca.zip -d $CERTIFICATE_PATH &&
                docker exec -i elasticsearch elasticsearch-certutil cert --ca-cert $CERTIFICATE_PATH/ca.crt --ca-key $CERTIFICATE_PATH/ca.key -pem --out $CERTIFICATE_PATH/certificates.zip &&
                docker exec -i elasticsearch unzip -j $CERTIFICATE_PATH/certificates.zip -d $CERTIFICATE_PATH &&
                docker exec -i elasticsearch rm $CERTIFICATE_PATH/certificates.zip $CERTIFICATE_PATH/my_ca.zip &&
                docker cp elasticsearch:$CERTIFICATE_PATH $DATA_PATH_STATE_REPO/
                echo "Test de la connexion ElasticSearch ..."
                ELASTIC_PASSWORD=$(cat $DATA_PATH_STATE_REPO/elastic | grep "New value:" | awk '{print $3}')
                docker exec -ti elasticsearch bin/elasticsearch-service-tokens create elastic/kibana kibana_token >> $DATA_PATH_STATE_REPO/elastic
                KIBANA_SERVICE_TOKEN=$(cat $DATA_PATH_STATE_REPO/elastic | grep "SERVICE_TOKEN" | awk '{print $4 }' | tr -d '\n' | tr -d '\r')
                echo "elasticsearch.serviceAccountToken: \"$KIBANA_SERVICE_TOKEN\" " >> $DATA_PATH_STATE_REPO/kibana.yml
                curl --cacert http_ca.crt -u elastic:$ELASTIC_PASSWORD https://localhost:9200
            else
                echo "Le dossier n'existe pas"
            fi
    else 
        echo "Error : Dockstack deploy"
    fi


}
function expose()
{
    cloudshell get-web-preview-url -p 3005
    cloudshell get-web-preview-url -p 5601
}

function install_alias()
{
    verify_existant_element__v3 e /root/.bashrc 1 0  && touch /root/.bashrc
    if  ! grep -Fxq "#dockstack:true" ~/.bashrc
    then
        echo "#dockstack:true">> ~/.bashrc;
        echo "Creation des alias pour dockstack";
        echo "alias dockstack=\"bash $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh dockstack\"" >> /root/.bashrc
        echo "alias dks=\"bash $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh dockstack\"" >> /root/.bashrc
        echo "alias dkss=\"bash $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh dockstack reset --deployment\"" >> /root/.bashrc
        echo 'alias resetall="docker-compose down --volumes && dockstack deploy && cloudshell get-web-preview-url -p 5601"' >> /root/.bashrc
        echo 'alias l="docker-compose logs elasticsearch"' >> /root/.bashrc
        echo 'alias k="docker-compose logs kibana"' >> /root/.bashrc
        echo 'alias es="docker exec -ti elasticsearch /bin/bash"' >> /root/.bashrc
        echo 'alias estoken="docker exec -ti elasticsearch bin/elasticsearch-create-enrollment-token --scope kibana"' >> /root/.bashrc
        echo 'alias esuser="echo 'y' | docker exec -i elasticsearch bin/elasticsearch-reset-password -u elastic"' >> /root/.bashrc
        echo 'alias kib="docker exec -ti kibana /bin/bash"' >> /root/.bashrc
        echo 'alias kibcode="docker exec -ti kibana bin/kibana-verification-code"' >> /root/.bashrc
        echo 'alias kcode="docker exec -ti kibana bin/kibana-verification-code"' >> /root/.bashrc
        echo 'alias kibview="cloudshell get-web-preview-url -p 5601"' >> /root/.bashrc
        echo 'alias kview="cloudshell get-web-preview-url -p 5601"' >> /root/.bashrc
        echo 'alias mview="cloudshell get-web-preview-url -p 8081"' >> /root/.bashrc
        echo 'alias gview="cloudshell get-web-preview-url -p 3005"' >> /root/.bashrc
        echo 'alias expose="cloudshell get-web-preview-url -p 5601"' >> /root/.bashrc
        echo 'alias reses="docker-compose restart elasticsearch"' >> /root/.bashrc
        echo 'alias n="nano docker-compose.yml"' >> /root/.bashrc
        echo 'alias reskib="docker-compose restart kibana"' >> /root/.bashrc
        echo 'alias edit="nano /root/.bashrc"' >> /root/.bashrc
        echo 'alias src="source /root/.bashrc"' >> /root/.bashrc
        echo 'alias er="rm /root/.bashrc"' >> /root/.bashrc
        echo 'alias dps="docker ps -a"' >> /root/.bashrc
        echo "alias gokc=\"cd $DATA_PATH_STATE_REPO/kibanaconfig && ls \"" >> /root/.bashrc
        echo "alias gokd=\"cd $DATA_PATH_STATE_REPO/kibanadata && ls\"" >> /root/.bashrc
        echo "alias goed=\"cd $DATA_PATH_STATE_REPO/esdata && ls\"" >> /root/.bashrc
        echo "alias goh=\"cd $DATA_PATH_STATE_REPO && ls\"" >> /root/.bashrc
        echo "alias resd=\"docker-compose down && docker-compose up -d\"" >> /root/.bashrc
        echo "#end_docker_alias:true">> ~/.bashrc;
    fi
    source /root/.bashrc
    chmod 777 /root/.bashrc
}



function ngrok_verif()
{
    output=$(ngrok config check 2>/dev/null)
    if [ $? -ne 0 ]
    then
        echo "Vous avez besoin de créer un compte ngrok"
        echo "Une fois créé, exécutez la commande suivante : ngrok config add-authtoken your_token"
        echo "Pour obtenir le token, allez sur le site https://ngrok.com/"
        exit 1 
    else
        if [ -z "$1" ]
        then 
            return 0
        else
            ngrok http $1
        fi
    fi
    return 0
}


function expose_services()
{
	SERVICE_NAME="ngrok"
	START_COMMAND="ngrok start --config $DATA_PATH_STATE_REPO/ngrok.yml --all"
	PID=$(pgrep $SERVICE_NAME)
	check_command_url="eval curl -s localhost:4040/api/tunnels  | jq '.tunnels[] | {public_url: .public_url, service_locale: .config.addr}'"
	if [[ -z $PID  && "$1" == "start" ]]
	then
        echo "Exposition des services"
		echo "$SERVICE_NAME n'est pas en cours d'exécution. Démarrage en arrière-plan..."
		$START_COMMAND > /dev/null 2>&1 &
		if [[ $? -eq 0 ]]
		then
			echo "$SERVICE_NAME a été démarré en arrière-plan avec succès."
            sleep 5 &&
			echo "Liste des URLs exposée : " &&
			$check_command_url
		else
			echo "Une erreur s'est produite lors du démarrage de $SERVICE_NAME."
		fi
	elif [[ "$1" == "stop" ]]
    then
		if [[ ! -z $PID ]]
		then
			echo "$SERVICE_NAME est en cours d'exécution avec PID $PID. Arrêt en cours..."
			kill $PID
			if [[ $? -eq 0 ]]
			then
				echo "$SERVICE_NAME a été arrêté avec succès."
			else
				echo "Une erreur s'est produite lors de l'arrêt de $SERVICE_NAME."
			fi
		else
			echo "$SERVICE_NAME n'est pas en cours d'exécution."
		fi
	elif [[ "$1" == "view" && "$2" == "urls" ]]
    then
            if [[ ! -z $PID  ]]
            then
                echo "Liste des URLs exposée : "
                $check_command_url
            else
                echo "Le service n'est pas lancé"
            fi
	elif [[ "$1" == "edit" ]]
    then
        nano $DATA_PATH_STATE_REPO/ngrok.yml
	else
            if [[ ! -z $PID && "$1" == "start" ]]
            then
                echo "Le service est  déjà  en cours d'exécution"
                echo "Liste des URLs exposée : "
                $check_command_url
            else
                echo "Service Expose Commands Error "
            fi
		
	fi
}
function insert_data()
{
        cat $1 | jq -c '.[]' | while read line; do
            curl -X POST "localhost:9200/$2/_doc/" -H "Content-Type: application/json" -d "$line"
        done
}
function configureMegaEnv()
{
    sed -i "s/YOUR_MEGA_EMAIL/$1/g" $DATA_PATH_STATE_REPO/.env
    escaped_x=$(printf '%s\n' "$2" | sed -e 's/[\/&]/\\&/g')
    sed -i "s/^MEGA_PASSWORD=.*$/MEGA_PASSWORD='$escaped_x'/g" $DATA_PATH_STATE_REPO/.env
}
function dockstack()
    {
        if [[ "$1" == "deploy" && "$2" == "" && "$3" == "" || "$1" == "--d" && "$2" == "" && "$3" == "" ]]
        then
            #Inclusion des process de vérification en dehors des VM Google Cloud Shell via require.sh
            wget  -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/require.sh
            dos2unix *.sh > /dev/null 2>&1
            chmod +x $DATA_PATH_STATE_REPO/require.sh
            bash $DATA_PATH_STATE_REPO/require.sh
            deploy_stack
        elif [[ "$1" == "deploy" && "$2" == "from" && "$3" == "last-session" && "$4" == "" ]]
        then
            deploy_stack from-cloud
        elif [[ "$1" == "restart" && "$2" == "stack" && "$3" == "containers" && "$4" == "" ]]
        then
            deploy_stack reset
        elif [[ "$1" == "deploy" && "$2" == "from" && "$3" == "last-session" && "$4" == "--email" && "$5" != "" && "$6" == "--password" && "$7" != "" ]]
        then
            configureMegaEnv $5 $7 && 
            deploy_stack from-cloud
        elif [[ "$1" == "start" && "$2" == "sync" && "$3" == "" ]]
        then
             bash $DATA_PATH_STATE_REPO/backup.sh start sync
        elif [[ "$1" == "stop" && "$2" == "sync" && "$3" == "" ]]
        then
             bash $DATA_PATH_STATE_REPO/backup.sh stop sync
        elif [[ "$1" == "reset" && "$2" == "--deployment" ]]
        then
           echo "Arret et supprésion des container"
           (cd $DATA_PATH_STATE_REPO ; docker-compose down --volumes)
           #find $DATA_PATH_STATE_REPO/* -path $DATA_PATH_STATE_REPO/dockstack -prune -o -exec rm -rf {} +
           echo "Redeploiement ..."
           deploy_stack
        elif [ "$1" == "show" ]
        then
            cat $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh  
        elif [[ "$1" == "create" && "$2" == "kibana" && "$3" == "--token" ]]
        then
            docker exec -ti elasticsearch bin/elasticsearch-create-enrollment-token --scope kibana
        elif [[ "$1" == "create" && "$2" == "kibana" && "$3" == "--code" ]]
        then
            docker exec -ti kibana bin/kibana-verification-code
        elif [[ "$1" == "view" &&  "$2" == "" ]]
        then
            expose
        elif [[ "$1" == "exec" && "$2" == "kibana" ]]
        then
            docker exec -ti kibana /bin/bash
        elif [[ "$1" == "exec" && "$2" == "elastic" || "$1" == "exec" && "$2" == "elasticsearch" ]]
        then
            docker exec -ti elasticsearch /bin/bash
        elif [[ "$1" == "exec" && "$2" == "elastic" || "$1" == "exec" && "$2" == "grafana" ]]
        then
            docker exec -ti grafana /bin/bash
        elif [[ "$1" == "reset" && "$2" == "--container"  ]]
        then
           docker stop $(docker ps -aq) && docker rm $(docker ps -aq)
        elif [[ "$1" == "reset" && "$2" == "--all"  ]]
        then
           docker stop $(docker ps -aq) && docker rm $(docker ps -aq) #&& docker network rm $(docker network ls -q)
        elif [[ "$1" == "logs" && "$2" != ""  ]]
        then
            docker-compose logs $2
        elif [[ "$1" == "test" && "$2" == "elastic"  ]]
        then
            curl --cacert http_ca.crt -u elastic https://localhost:9200 
        elif [[ "$1" == "insert" && "$2" != "" && "$3" != "" && "$4" == "" ]]
        then
            if [[ "$2" == *.json ]]
            then
                insert_data $2 $3
            elif [[ "$2" == *.csv ]]
            then
                json_file="${2%.csv}.json"
                python3 transform.py "$2" "$json_file"
                insert_data $json_file $3
            else
                echo "Seulement fichier CSV et JSON apcepté"
                exit 1
            fi
        elif [[ "$1" == "insert" && "$2" == "--all" && "$3" != "" && "$4" != "" ]]
        then
            # Si $3 est un fichier ZIP
            echo "insert all"
            if [[ "$3" == *.zip ]]
            then
                DIRNAME=$(basename "$3" .zip)
                unzip -o "$3" -d "$DATA_PATH_STATE_REPO/$DIRNAME"
                TARGET_DIRECTORY="$DATA_PATH_STATE_REPO/$DIRNAME"
            else
                TARGET_DIRECTORY="$DATA_PATH_STATE_REPO/$3"
                echo $TARGET_DIRECTORY
            fi
            shopt -s nullglob 
            for file in $TARGET_DIRECTORY/*.csv $TARGET_DIRECTORY/*.json
            do
                    if [[ "$file" == *.json ]]
                    then
                        insert_data "$file" "$4"
                    elif [[ "$file" == *.csv ]]
                    then
                        json_filename=$(basename "$file" .csv).json #Pour la création du future fichier JSOn basée sur le nom du CSV
                        python3 transform.py "$file" "$TARGET_DIRECTORY/$json_filename"
                        insert_data "$TARGET_DIRECTORY/$json_filename" "$4"
                    else
                        echo "Type de fichier non reconnu: $file"
                        exit 1
                    fi
             done
             shopt -u nullglob
        elif [[ "$1" == "smartwatch" && "$2" == "from" && "$3" != "" && "$4" == "--index" && "$5" != "" ]]
        then
            # Vérifiez si le script est déjà en cours d'exécution
            if pgrep -f "watch.sh" > /dev/null; then
                echo "Smart Watch est déjà activé"
            else
                bash $DATA_PATH_STATE_REPO/watch.sh $3 $5 >  $DATA_PATH_STATE_REPO/watch_logs.txt 2>&1 &
                echo "Smart Watch activé"
            fi
        elif [[ "$1" == "smartwatch" && "$2" == "stop" ]]
        then
            # Trouver et tuer le processus
            if pgrep -f "watch.sh" > /dev/null; then
                pkill -f "watch.sh"
                echo "Smart Watch est arrêté"
            else
                echo "Smart Watch n'est pas en cours d'exécution"
            fi
        elif [[ "$1" == "update" && "$2" == "" || "$1" == "--u" && "$2" == "" ]]
        then
            install_alias
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/datasources.yaml 0 1 &&  rm $DATA_PATH_STATE_REPO/datasources.yaml
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/kibana.yml 0 1 && rm $DATA_PATH_STATE_REPO/kibana.yml
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/docker-compose.yml 0 1 && rm $DATA_PATH_STATE_REPO/docker-compose.yml
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/backup.sh 0 1 && rm $DATA_PATH_STATE_REPO/backup.sh
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/watch.sh 0 1 && rm $DATA_PATH_STATE_REPO/watch.sh
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/require.sh 0 1 && rm $DATA_PATH_STATE_REPO/require.sh
            #verify_existant_element__v3 e $DATA_PATH_STATE_REPO/.env 0 1 && rm $DATA_PATH_STATE_REPO/.env
            echo "Téléchargement des fichiers"
            downloadFile
            echo "Mise à jour de stackDeployment.sh"
            verify_existant_element__v3 d $DATA_PATH_STATE_REPO/dockstack 1 0 --v && mkdir $DATA_PATH_STATE_REPO/dockstack
            rm $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh
            wget  -q --timestamping -P $DATA_PATH_STATE_REPO/dockstack https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/stackDeployment.sh
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh 0 1 && ( cd $DATA_PATH_STATE_REPO/dockstack ; chmod +777 stackDeployment.sh; ) && echo "Mise à jour réussie ! "
            echo -e "$VERSION"
        elif [[ "$1" == "update" && "$2" == "--all" || "$1" == "--u" && "$2" == "--all" ]]
        then
              install_alias
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/datasources.yaml 0 1 &&  rm $DATA_PATH_STATE_REPO/datasources.yaml
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/kibana.yml 0 1 && rm $DATA_PATH_STATE_REPO/kibana.yml
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/docker-compose.yml 0 1 && rm $DATA_PATH_STATE_REPO/docker-compose.yml
            #verify_existant_element__v3 e $DATA_PATH_STATE_REPO/.env 0 1 && rm $DATA_PATH_STATE_REPO/.env
            echo "Mise à jour de tous les éléments"
            echo "Téléchargement des fichiers"
            downloadFile $2
            echo "Mise à jour de stackDeployment.sh"
            verify_existant_element__v3 d $DATA_PATH_STATE_REPO/dockstack 1 0 --v && mkdir $DATA_PATH_STATE_REPO/dockstack
            rm $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh
            wget  -q --timestamping -P $DATA_PATH_STATE_REPO/dockstack https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/stackDeployment.sh
            verify_existant_element__v3 e $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh 0 1 && ( cd $DATA_PATH_STATE_REPO/dockstack ; chmod +777 stackDeployment.sh; ) && echo "Mise à jour réussie ! "           
            echo -e $VERSION
        elif [[ "$1" == "expose" && "$2" == "grafana" ]]
        then
                echo "Exposition de Grafana  $1  $2  $3 $4"
                ngrok_verif 3005
        elif [[ "$1" == "expose" && "$2" == "kibana" ]]
        then
                echo "Exposition de Kibana"
                ngrok_verif 5601
        elif [[ "$1" == "expose-services" && "$2" != "" || "$1" == "expose-services" && "$2" != "" && "$3" != "" ]]
        then
                ngrok_verif && NGROK_AUTH_TOKEN=$(cat /root/.config/ngrok/ngrok.yml | grep "authtoken" | awk '{print $2}') && 
                sed -i "s/your_token/$NGROK_AUTH_TOKEN/" $DATA_PATH_STATE_REPO/ngrok.yml &&
                expose_services $2 $3
        elif [[ "$1" == "mount" && "$2" == "services" ]]
        then
                echo "Lancement des service"
                chmod -R 777 $DATA_PATH_STATE_REPO
                (cd $DATA_PATH_STATE_REPO ; docker-compose --profile required up -d  ) 
        elif [[ "$1" == "version" || "$1" == "--v" ]]
        then
                echo -e $VERSION
        elif [[ "$1" == "view" && "$2" == "urls" ]]
        then
          $check_command_url
        elif [[ "$1" == "erase" || "$1" == "--all" ]]
        then
            echo "Copie du programme ..."
            mv  dockstack /tmp
            echo "Suppresion de tout les fichier"
            rm -r *
            echo "Restauration du programme ..."
            mv  /tmp/dockstack $DATA_PATH_STATE_REPO
        elif [[ "$1" == "create" && "$2" == "kubernetes" && "$3" == "cluster" ]]
        then
             kind create cluster --config $DATA_PATH_STATE_REPO/kubernetes_cluster.yml
        fi
}
function init()
{
    # Vérifie si dos2unix est installé
    if ! command -v dos2unix &> /dev/null; then
        echo "dos2unix n'est pas installé sur le système."
        echo "Installation en cours..."
        sudo apt-get update && sudo apt-get install dos2unix
        if [ $? -eq 0 ]; then
            echo "dos2unix a été installé avec succès."
            dos2unix *.sh > /dev/null 2>&1
        else
            echo "Une erreur s'est produite lors de l'installation de dos2unix."
            exit 1
        fi
    else
        echo "dos2unix est déjà installé sur votre système."
        dos2unix *.sh > /dev/null 2>&1
    fi
    verify_existant_element__v3 d $DATA_PATH_STATE_REPO/dockstack 1 0  && echo "Création du dossier" && mkdir $DATA_PATH_STATE_REPO/dockstack
    verify_existant_element__v3 e $DATA_PATH_STATE_REPO/dockstack/stackDeployment.sh 1 0  && ( cd $DATA_PATH_STATE_REPO/dockstack/ ; wget -q https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/stackDeployment.sh && chmod +777 stackDeployment.sh; )
    install_alias
    verify_existant_element__v3 e $DATA_PATH_STATE_REPO/stackDeployment.sh 0 1 && rm stackDeployment.sh 
    


    if ! command -v "ngrok" &> /dev/null
    then
            wget --timestamping -P $DATA_PATH_STATE_REPO https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-amd64.tgz


            tar xvzf $DATA_PATH_STATE_REPO/ngrok-v3-stable-linux-amd64.tgz -C /usr/local/bin
    fi
        
        
        verify_existant_element__v3 e "$DATA_PATH_STATE_REPO"/commands.sh 1 0 &&
        wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/commands.sh &&
        chmod +x  $DATA_PATH_STATE_REPO/commands.sh

        verify_existant_element__v3 e "$DATA_PATH_STATE_REPO"/.env 1 0 &&
        wget -q --timestamping -P $DATA_PATH_STATE_REPO https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/.env
}
init
"$@"
