import os
import csv
import json
import sys


def get_latest_file(path):
    """Récupère le chemin du fichier le plus récent dans le répertoire spécifié."""
    all_files = [os.path.join(path, f) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    return max(all_files, key=os.path.getctime)


def csv_to_json_data(csv_file_path):
    """Convertit les données CSV en JSON."""
    data = []
    
    with open(csv_file_path, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            data.append(row)
    
    return data


def transform_data(data):
    """Transforme les clés et les valeurs."""
    transformed_data_list = []
    for item in data:
        transformed_data = {}
        for key, value in item.items():
            transformed_key = key.strip("\"'")
            transformed_value = value.strip("\"'")
            transformed_data[transformed_key] = transformed_value
        transformed_data_list.append(transformed_data)
    return transformed_data_list


def main():
    if len(sys.argv) != 3:
        print("Usage: python3 program.py <directory_path> <output.json>")
        sys.exit(1)
    
    directory_path = sys.argv[1]
    json_file_path = sys.argv[2]
    
    last_file = get_latest_file(directory_path)

    # Convert CSV to JSON
    data = csv_to_json_data(last_file)
    transformed_data = transform_data(data)
    
    with open(json_file_path, 'w') as json_file:
        for item in transformed_data:
            json.dump(item, json_file)
            json_file.write('\n')
    
    print(f"Les données de {last_file} ont été converties en {json_file_path}")


main()
