events {}

stream {
    server {
        listen  27020 so_keepalive=on;
        proxy_connect_timeout 2s;
        proxy_pass    stream_mongo_backend;
        proxy_timeout 10m;
    }

    upstream stream_mongo_backend {
        server mongodb:27017;
    }
}

http {
    upstream elasticsearch {
        server elasticsearch:9200; 
        keepalive 15;
    }

    upstream grafana {
        server grafana:3000;
        keepalive 15;
    }

    upstream kong {
        server kong:8000;
        keepalive 15;
    }

    upstream prometheus {
        server prometheus:9090; 
        keepalive 15;
    }

    upstream artifactory {
        server artifactory:8081;
        keepalive 15;
    }

    # Serveur pour le proxy pass normal
    server {
        listen 80;

        location / {
            proxy_pass http://kong/;
            proxy_http_version 1.1;
            proxy_set_header Connection "Keep-Alive";
            proxy_set_header Proxy-Connection "Keep-Alive";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /elasticsearch/ {
            proxy_pass http://elasticsearch/;
            proxy_http_version 1.1;
            proxy_set_header Connection "Keep-Alive";
            proxy_set_header Proxy-Connection "Keep-Alive";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /grafana/ {
            proxy_pass http://grafana/;
            proxy_http_version 1.1;
            proxy_set_header Connection "Keep-Alive";
            proxy_set_header Proxy-Connection "Keep-Alive";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /prometheus/ {
            proxy_pass http://prometheus/;
            proxy_redirect default;
            proxy_redirect /graph /prometheus/graph;
            proxy_http_version 1.1;
            proxy_set_header Connection "Keep-Alive";
            proxy_set_header Proxy-Connection "Keep-Alive";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /gitlab/ {
            proxy_pass http://gitlab/gitlab/; #le http://gitlab donne accès au container, le /gitlab suivant donne accès a gitlab via la config du docker-compose
            proxy_http_version 1.1;
            proxy_set_header Connection "Keep-Alive";
            proxy_set_header Proxy-Connection "Keep-Alive";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /artifactory/ {
            proxy_pass http://artifactory/artifactory/;
            proxy_http_version 1.1;
            proxy_set_header Connection "Keep-Alive";
            proxy_set_header Proxy-Connection "Keep-Alive";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
        
        location /home/ {
            root /usr/share/nginx/html;
        }
    }

    # Serveur pour le statut Nginx
    server {
        listen 8080; # Port pour le statut Nginx

        location /nginx_status {
            stub_status on;
            allow all; # Autorisez uniquement l'accès local
            #deny all;
        }
    }
}
