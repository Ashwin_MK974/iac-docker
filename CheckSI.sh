#!/bin/bash
index="check_si_status_code"
# check_url "https://fs-app-amk.netlify.app/" "Page d'accueil" "Page Principale du site"
function check_url() {
    CURRENT_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
    RESULT_CODE=$(curl --insecure -I -s -L -o /dev/null -w '%{http_code}' $1)

    if [ $? -ne 0 ]; then
        echo "Erreur lors de la récupération du code de statut pour l'URL $1"
        return 1
    fi

    echo $RESULT_CODE
    echo $CURRENT_DATE

    curl -X POST "http://localhost:9200/$index/_doc" \
    -H 'Content-Type: application/json' \
    -d'{
      "status_code": '"$RESULT_CODE"',
      "page_name": "'"$2"'",
      "page_description": "'"$3"'",
      "page_url": "'"$1"'",
      "timestamp": "'"$CURRENT_DATE"'"
    }'
}

function createIndex()
{
    curl -X PUT "http://localhost:9200/$index" -H 'Content-Type: application/json' -d'
{
  "mappings": {
    "properties": {
      "status_code": { "type": "integer" },
      "page_name": { "type": "keyword" },
      "page_description": { "type": "text" },
      "page_url": { "type": "text" },
      "timestamp": { "type": "date" }
    }
  }
}'

}
function check_index()
{
    response=$(curl -I -s -o /dev/null  -w "%{http_code}\n" http://localhost:9200/$index)
    if [ "$response" == "200" ]; then
        echo "L'index existe."
    else
        echo "L'index n'existe pas. Création de l'index"
        createIndex
    fi
}
check_index $index
function Main()
{
    
    check_url "https://fs-app-amk.netlify.app/" "Page d'accueil" "Page Principale du site"

}

while true
do
    Main
sleep 60
done
