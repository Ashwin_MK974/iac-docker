# DockStack : Elastic , Kibana , Grafana , MongoDB , MongoDB Express, Prometheus, Thanos, Kubernetes, Ansible, Terraform, Gitlab

Ce projet facilite le déploiement d'une instance comprenant Elastic, Kibana et Grafana. Il est spécialement conçu pour fonctionner dans l'environnement Google Cloud Shell Editor, où les données sont stockées de manière temporaire. 



#### Démarrage
Si l'environnement temporaire de Google Cloud Shell Editor est utilisé, DockStack doit être exécuté dans le répertoire personnel. Sinon, les données ne seront pas persistées à la prochaine connexion.

Dockstack prend en charge la création de tous les répertoires nécessaires à la persistance des données dans le répertoire personnel.
Le programme écrit des alias dans /root/.bashrc.

Installation : 


```
export PATH_INSTALL=$PWD
wget -q https://gitlab.com/Ashwin_MK974/iac-docker/-/raw/main/stackDeployment.sh
chmod +x stackDeployment.sh
./stackDeployment.sh
source /root/.bashrc
 
```


Pour déployer la stack complete : 
```
dockstack  deploy
```
Pour déployer à partir d'une sauvegarde  : 
```
dockstack  deploy from last-session
```
Cela nécessite à ce que les identifiant de connexion de MegaCloud soient définis au niveau du fichier .environnement
L'autre commande permet de démarrer le déploiement en une seule ligne en spécifiant les identifiant de connexions.
Le mot de pase doit être spécifié entre des single quotes ==> ''
```
dockstack deploy from last-session --email your@email.com --password 'your@password'
```
Pour commencer la sauvegarde des données du programme avec les toutes les données générées par les containeurs, il faut utiliser l'option sync.
```
dockstack start sync
```
Les données générées par les containers dans les volumes sont donc sauvegarder vers le cloud.
Cependant la sauvegarde est contraint à des problèmes d'integrité. Cela peut se produire lorsque la sauvegarde est arrêté et les containers sont en cours de fonctionnement. Cela intervient également lorsque lorsque la VM est arrêté, mais les données des volumes n'ont pas été correctement transferée. Cela peut poser alors des problèmes de  desynchronisation de la sauvegarde qui porte atteinte à l'integrité des données. Les containeurs ne peuvent plus alors démarrée correctement si l'on repart à partir d'une sauvegarde. Pour éviter cela, il faut alors stopper touts les container via "docker-compose down", puis atendre que le transfère s'effectue correctement.

La commande permettant de réaliser cette opération est la suivante:
```
dockstack stop sync
```
Elle peut donc mettre plusieurs minutes avant de s'arrêter.


Pour avoir l'url permettant d'accèder à l'interface Kibana
```
kview
```
Pour avoir l'url permettant d'accèder à l'interface Grafana
```
gview
```
Pour avoir l'url permettant d'accèder à l'interface MongoDB
```
mview
```
Pour accèder à Grafana via une URL Internet
```
dockstack expose grafana
```
Pour accèder à Kibana via une URL Internet
```
dockstack expose Kibana
```

Pour accèder à tous les services via des URL Internet
```
dockstack expose-services start
```
Pour arrêter l'exposition des services
```
dockstack expose-services stop
```
Pour voir les URLs exposées
```
dockstack expose-services view urls
```
Pour modifier l'exposition des services
```
dockstack expose-services edit
```
Pour ajouter des données  à l'instance elastic dans un index spécifique
La fonction insert apcepte des données JSON ou CSV
Pour les fichiers JSON
```
dockstack insert data.json  my_index
```
Pour les fichiers CSV
```
 dockstack insert data.csv  my_index 
```
🛈 DockStack peut injecter plusieurs fichiers CSV ou JSON provenant d'un fichier zip ou d'un répertoire.
Il faut indiquer le répertoire local ainsi l'index Elastic dans laquelle on souhaite insérer des données
```
dockstack insert --all path/data my_index
```
Pour insérer des données JSON ou CSV provenant d'un fichier ZIP on indique alors simplement le fichier ZIP contenant les données ainsi que l'index Elastic
```
dockstack insert --all path/data.zip my_index
```
Le programme détecte automatiquement les fichiers CSV ou JSON. Dans le cas des fichiers CSV, une transformation en JSON est effectuée. Ensuite, le programme récupère chaque objet du fichier JSON et le convertit en document pour l'injecter dans un index Elasticsearch

Pour surveiller de facon automatique l'ajout de nouveaux fichiers à des répertoires afin de les traiter dans Elastic, Dockstack intégre une fonctionalité de surveillance auto appelé smartwatch.
Chaque nouveaux fichiers qui est ajouté à un repertoire, est surveiller par smartwatch. Une conversion est realisé en JSON avant d'être inseré dans un index Elastic.
La commade prend trois arguments, le premier le repertoire a surveiller qui contient les csv, le deuxième sert a préciser l'index , le troisième contient le nom de l'index.
SmartWatch fais un hash des fichiers après chaque insertion, afin qu'aux redemarrage du système, il se base sur les hash effectué, pour ne pas reinjecter de même donnée dans l'index Elastic.
```
dockstack smartwatch from repo/ --index example_elastic_index
```

Pour redéployer complètement les instances
```
dockstack reset --deployment 
```

## Notes

Lorsque les instances sont arrêtées, pour les redéployer, il faut exécuter la commande suivante :
```
dockstack mount services 
```
La commande : `dockstack deploy` redeploie en supprimant les volumes

Étant donné que DockStack repose principalement sur Docker, la commande équivalente est :
```
docker-compose up -d 
```
Commande pour déployer rapidement en intégrant les autres services :


cd stack/ && source commands.sh --direct-launch



🛈 Ngrok, dans sa version gratuite, limite l'utilisateur à trois tunnels simultanés. Pour pallier cette limitation, c'est là qu'intervient le serveur Nginx qui agit comme un reverse proxy. Grâce à cette configuration, un seul tunnel est créé depuis Ngrok vers le serveur Nginx, qui lui-même redirige les requêtes vers plusieurs services internes, tels que Kibana, Grafana et Elasticsearch. Cela permet de contourner la limitation tout en accédant à plusieurs services via une unique adresse Ngrok.
Exemple : https://testnet-init.ngrok-free.app/grafana , https://testnet-init.ngrok-free.app/kibana

🛈Pour exposer les services via Internet, un token d'authentification Ngrok est nécessaire. Lors de l'exécution de la commande `dockstack expose-services start` pour la première fois, veuillez suivre les instructions affichées. Les services pouvant être exposés à Internet sont définis et peuvent être modifiés dans le fichier ngrok.yml

Pour ajouter un token d'authentification la commande est la suivante :

```
 ngrok config add-authtoken your_token
```

🛈À l'exception de Grafana, les instances d'Elastic ainsi que Kibana, ne disposent pas de certificats ni d'authentification. Il est donc déconseillé de les exposer sur Internet.  
Pour cela il est recommandé de les exposer en local via une authentification Google Cloud via la commande  `kview`.  
La version actuelle de DockStack ne prend pas en charge la génération automatique de certificats pour EK.

Red Hat 8.9 testnet
```
docker pull redhat/ubi8:8.9 && docker run -it redhat/ubi8:8.9 /bin/bash
```


## Architecture de DockStack
![](./Dockstack_Architecture.png)
