#!/bin/bash
# Chemin personnalisé pour les données Docker
if [[ -z "$1" ]]
then
  DOCKER_DATA_DIR="$HOME/docker"
else
  DOCKER_DATA_DIR="$1"
fi
DOCKER_CONFIG_FILE="/etc/docker/daemon.json"

# Vérifier les permissions root
if [ "$EUID" -ne 0 ]; then
  echo "❌ Veuillez exécuter ce script en tant que root ou avec sudo."
  exit 1
fi

# Étape 1 : Créer le répertoire de stockage personnalisé
echo "📂 Création du répertoire personnalisé pour Docker..."
mkdir -p "$DOCKER_DATA_DIR"
if [ $? -ne 0 ]; then
  echo "❌ Échec de la création du répertoire $DOCKER_DATA_DIR."
  exit 1
fi
echo "✅ Répertoire $DOCKER_DATA_DIR créé avec succès."

# Étape 2 : Configurer le fichier daemon.json
echo "🛠️  Configuration du fichier $DOCKER_CONFIG_FILE..."
if [ ! -f "$DOCKER_CONFIG_FILE" ]; then
  echo "{}" > "$DOCKER_CONFIG_FILE"
fi

# Ajouter ou modifier le champ "data-root"
jq '. + {"data-root": "'$DOCKER_DATA_DIR'"}' "$DOCKER_CONFIG_FILE" > /tmp/daemon.json && mv /tmp/daemon.json "$DOCKER_CONFIG_FILE"

if [ $? -ne 0 ]; then
  echo "❌ Échec de la configuration du fichier $DOCKER_CONFIG_FILE."
  exit 1
fi
echo "✅ Fichier $DOCKER_CONFIG_FILE configuré avec succès."

# Étape 3 : Redémarrer Docker sans systemctl
echo "🔄 Redémarrage de Docker..."
pkill dockerd
if [ $? -eq 0 ]; then
  echo "✅ Docker arrêté avec succès."
else
  echo "⚠️  Docker ne semble pas être en cours d'exécution."
fi

# Essayer de démarrer Docker deux fois en cas d'échec initial
for attempt in 1 2; do
  echo "🔄 Tentative de redémarrage $attempt..."
  nohup dockerd > /var/log/dockerd.log 2>&1 &
  sleep 5  # Attendre que Docker démarre

  # Vérifier si Docker fonctionne
  docker info > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "✅ Docker redémarré avec succès à la tentative $attempt."
    break
  elif [ $attempt -eq 2 ]; then
    echo "❌ Échec du redémarrage de Docker après deux tentatives. Vérifiez les logs dans /var/log/dockerd.log."
    exit 1
  fi
done

# Étape 4 : Vérification du nouvel emplacement
echo "📂 Vérification de l'emplacement des données Docker..."
if [ -d "$DOCKER_DATA_DIR" ]; then
  echo "✅ Docker utilise désormais $DOCKER_DATA_DIR pour ses données."
else
  echo "❌ Le répertoire $DOCKER_DATA_DIR n'est pas utilisé. Vérifiez la configuration."
  exit 1
fi

echo "🎉 Configuration de Docker terminée avec succès."
exit 0
