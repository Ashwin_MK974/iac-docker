import csv
import json
import sys

def csv_to_json_data(csv_file_path):
    """Convert CSV data to JSON."""
    data = []
    
    with open(csv_file_path, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            data.append(row)
    
    return data

def transform_data(data):
    """Transformer les clés et les valeurs."""
    transformed_data_list = []
    for item in data:
        transformed_data = {}
        for key, value in item.items():
            # Supprimer les guillemets superflus
            transformed_key = key.replace("$", "").replace("(", "").replace(")", "").strip("\"'").lower()
            transformed_value = value.replace("$", "").replace("(", "").replace(")", "").strip("\"'").lower()
            transformed_data[transformed_key] = transformed_value
        transformed_data_list.append(transformed_data)

    return transformed_data_list

def main():
    if len(sys.argv) != 3:
        print("Usage: python3 program.py <input.csv> <output.json>")
        sys.exit(1)
    
    csv_file_path = sys.argv[1]
    json_file_path = sys.argv[2]
    
    # Convertir CSV en JSON
    data = csv_to_json_data(csv_file_path)
    
    # Transformer les données
    transformed_data = transform_data(data)
    
    # Sauvegarder le résultat transformé dans le fichier de sortie spécifié
    with open(json_file_path, 'w') as json_file:
        json.dump(transformed_data, json_file, indent=4)

    print(f"Les données de {csv_file_path} ont été converties et transformées avec succès en {json_file_path}")

if __name__ == "__main__":
    main()