#!/bin/bash
DATA_PATH_STATE_REPO=$PWD
source $DATA_PATH_STATE_REPO/commands.sh
source $DATA_PATH_STATE_REPO/.env
data_dir=$DATA_PATH_STATE_REPO
mega_dest="dockstack-backup"
lock_file="lock"

function megaLogin() {
    CHECK_SESSION=$(mega-whoami | grep "Not logged in")
    if [[ -n "$CHECK_SESSION" ]]
    then
        if [[ "$MEGA_PASSWORD" == "YOUR_MEGA_PASSWORD" ]]
        then
            echo "Veuillez définir les identifiants mega dans .env ou via dockstack deploy from last-session --email: mega@email.com --password: 'mega@password.com' "
            exit 1  
        else
            mega-login $MEGA_EMAIL $MEGA_PASSWORD
            if [[ $? -ne 0 ]]; then
                echo "Erreur lors de la connexion à MEGA. Vérifiez vos identifiants."
                exit 1  
            else
                echo "Connexion à MEGA réussie."
            fi
        fi
    else
       echo "Compte Mega Valide"
    fi
}


function init()
{

    
        #Téléchargement des prerequis avant l'installation de megacmd 
        
        # Télécharger libicu67 seulement s'il n'existe pas déjà
        if [ ! -f "$DATA_PATH_STATE_REPO/libicu67_67.1-7_amd64.deb" ]
        then
            echo "Téléchargement de libicu67_67.1-7_amd64.deb ..."
            wget -q -P "$DATA_PATH_STATE_REPO" http://ftp.de.debian.org/debian/pool/main/i/icu/libicu67_67.1-7_amd64.deb
        else
            echo "libicu67_67.1-7_amd64.deb déjà présent."
        fi

        # Télécharger libssl1.1 seulement s'il n'existe pas déjà
        if [ ! -f "$DATA_PATH_STATE_REPO/libssl1.1_1.1.1n-0+deb10u3_amd64.deb" ]
        then
            echo "Téléchargement de libssl1.1_1.1.1n-0+deb10u3_amd64.deb ..."
            wget -q -P "$DATA_PATH_STATE_REPO" https://ftp.debian.org/debian/pool/main/o/openssl/libssl1.1_1.1.1n-0+deb10u3_amd64.deb
        else
            echo "libssl1.1_1.1.1n-0+deb10u3_amd64.deb déjà présent."
        fi

        # Installer les paquets téléchargés
        sudo dpkg -i "$DATA_PATH_STATE_REPO/libicu67_67.1-7_amd64.deb"
        sudo dpkg -i "$DATA_PATH_STATE_REPO/libssl1.1_1.1.1n-0+deb10u3_amd64.deb"



    if ! command -v "mega-cmd" &> /dev/null
    then
            wget https://mega.nz/linux/repo/Debian_11/amd64/megacmd-Debian_11_amd64.deb && \
            sudo apt install -y "$PWD/megacmd-Debian_11_amd64.deb" && \
            sleep 10 && \
            megaLogin
    else
            echo "MEGAcmd est déjà installé."
            megaLogin
    fi
}


function check_backup_folder()
{
	folder_check=$(mega-ls | grep -w $mega_dest)
	if [[ -z $folder_check ]]
	then
		echo "Le dossier $mega_dest n'existe pas. Création en cours..."
		mega-mkdir  $mega_dest 
	else
		echo "Le dossier $mega_dest existe déjà."
	fi

}



items_to_backup=(
    ".env"
    "esdata"
    "grafanadata"
    "kibanadata"
    "mongodb-data"
    "kong-database"
    "elasticsearch.yml"
    "grafana.ini"
    "kibana.yml"
    "nginx.conf"
    "ngrok.yml"
)

function backup()
{
        echo "$data_dir/$item" "$mega_dest"
        mega-put "$data_dir/$1" "$mega_dest" &&
        echo "Sauvegarde terminée."
}

function check_rsync()
{
    if ! command -v "rsync" &> /dev/null
    then          
            sudo apt install -y rsync
            if [[ $? -ne 0 ]]
            then
                echo "Erreur lors de l'installation de rsync"
                exit 1
            fi
            sleep 5
    else
            echo "Rsync est déjà installé."
    fi
}

function getBackup {
    check_rsync
    mega-get $mega_dest /tmp
    if [[ $? -ne 0 ]]
    then
        echo "Erreur lors de la récupération des données de Mega. Arrêt du script."
        exit 1
    fi
    rsync -a --delete --exclude=.env /tmp/$mega_dest/ $DATA_PATH_STATE_REPO/
    chmod -R 777 $DATA_PATH_STATE_REPO/*
    if [[ $? -ne 0 ]]
    then
        echo "Erreur lors de la synchronisation des fichiers. Arrêt du script."
        exit 1
    fi
    if [[ -d "/tmp/$mega_dest" ]]
    then
        #rm -rf /tmp/$mega_dest/
        echo "no suppres"
    fi
    echo "Récuperation effectuée."
}



function syncBackup()
{
    mega-sync $DATA_PATH_STATE_REPO $mega_dest
}


function MainManageBackup()
{
    init
    if [[ "$1" == "create" && "$2" == "backup" && "$3" != "" ]];  # Corriger la syntaxe ici
    then
            check_backup_folder && backup $3
    elif [[ "$1" == "get" && "$2" == "backup" ]];  
    then
        getBackup
        cd "$DATA_PATH_STATE_REPO" && docker-compose up -d && sleep 10 && docker ps
    elif [[ "$1" == "start" && "$2" == "sync" ]];  
    then
        syncID=$(mega-sync | awk 'NR==2' | awk '{print $1}')
        if [[ ! -z "$syncID" ]]
        then
            echo "Session Mega-Sync déjà active"
        else
           check_backup_folder && syncBackup
        fi
    elif [[ "$1" == "stop" && "$2" == "sync" ]];  
    then
        syncID=$(mega-sync | awk 'NR==2' | awk '{print $1}')
        if [[ -z "$syncID" ]]
        then
            echo "Aucunne session Mega-Sync active"
        else
            mega-sync -d $syncID
        fi

    else
        echo "Usage:  {launch|create|get|sync} backup"  # Message d'usage en cas d'arguments incorrects
    fi
}

MainManageBackup "$@"  # Appeler la fonction avec les arguments du script
